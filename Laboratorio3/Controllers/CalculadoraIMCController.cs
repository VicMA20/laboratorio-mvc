﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Laboratorio3.Models;

namespace Laboratorio3.Controllers
{
    public class CalculadoraIMCController : Controller
    {

		public ActionResult ResultadoIMC()
		{
			PersonaModel persona = new PersonaModel(1, "Cristiano Ronaldo", 84.0, 1.87);
			double IMC = persona.Peso / (persona.Estatura * persona.Estatura);
			ViewBag.IMC = IMC;
			ViewBag.persona = persona;
			return View();
		}

		public ActionResult ResultadosAleatoriosIMC()
        {
			List <PersonaModel> personas = new List<PersonaModel>();
			Random random = new Random();

			for (int i = 0; i < 30; i++)
            {
				personas.Add(new PersonaModel {Id = i + 1, Nombre = "Sin nombre", Peso = random.NextDouble() * (150.0 - 20.0) + 20.0, Estatura = random.NextDouble() * (2.0 - 1.0) + 1.0 });			
				ViewBag.persona = personas;
			}


			return View();
        }
	}
}